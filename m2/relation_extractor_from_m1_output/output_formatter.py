def remove_entries_with_no_relation(json):
    result = {}
    for key in json.keys():
        if json[key]:
            result[key] = json[key]
    return result

def remove_meaningless_content(json):
    for defined_concept in json.keys():
        for relation in json[defined_concept].keys():
            if defined_concept in json[defined_concept][relation].keys():
                del json[defined_concept][relation][defined_concept]
    return json

def remove_empty_correspondent_concepts(json):
    result = {}
    for key in json.keys():
        for relation in json[key].keys():
            if json[key][relation]:
                if key not in result.keys():
                    result[key] = {}
                result[key][relation] = json[key][relation]
    return result

def remove_redundancies(json):
    result = remove_meaningless_content(json)
    result = remove_entries_with_no_relation(result)
    result = remove_empty_correspondent_concepts(result)
    return result